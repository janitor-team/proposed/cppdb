Source: cppdb
Priority: optional
Maintainer: Tobias Frost <tobi@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 12),
               default-libmysqlclient-dev,
               libpq-dev,
               libsqlite3-dev,
               unixodbc-dev
Rules-Requires-Root: no
Standards-Version: 4.2.1
Section: libs
Homepage: http://cppcms.com/wikipp/en/page/sql_connectivity
VCS-Git: https://salsa.debian.org/debian/cppdb.git
VCS-Browser: https://salsa.debian.org/debian/cppdb

Package: libcppdb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libcppdb-mysql0 (= ${binary:Version}),
         libcppdb-odbc0 (= ${binary:Version}),
         libcppdb-postgresql0 (= ${binary:Version}),
         libcppdb-sqlite3-0 (= ${binary:Version}),
         libcppdb0 (= ${binary:Version}),
         ${misc:Depends}
Description: SQL Connectivity Library (development files)
 CppDB is an SQL connectivity library that is designed to provide platform and
 Database independent connectivity API similarly to what JDBC, ODBC and other
 connectivity libraries do.
 .
 This library is developed as part of CppCMS Project - the C++ Web Development
 Framework.
 .
 CppDB was designed with following goals in the mind:
  - Performance is the primary goal - make fastest possible SQL connectivity as
    possible
  - Transparent connection pooling support
  - Transparent prepared statements caching
  - Dynamic DB modules loading and optional static linking
  - Full and high priority support of FOSS RDBMS: MySQL, PostgreSQL, Sqlite3
  - Support as many RDBMSs as possible via cppdb-odbc bridge
  - Simplicity in use
  - Locale safety
  - Support of both explicit verbose API and brief and nice syntactic sugar
 .
 This package contains the development files.

Package: libcppdb0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: libcppdb-mysql0,
          libcppdb-odbc0,
          libcppdb-postgresql0,
          libcppdb-sqlite3-0
Description: SQL Connectivity Library (core library)
 CppDB is an SQL connectivity library that is designed to provide platform and
 Database independent connectivity API similarly to what JDBC, ODBC and other
 connectivity libraries do.
 .
 This library is developed as part of CppCMS Project - the C++ Web Development
 Framework.
  .
 CppDB was designed with following goals in the mind:
  - Performance is the primary goal - make fastest possible SQL connectivity as
    possible
  - Transparent connection pooling support
  - Transparent prepared statements caching
  - Dynamic DB modules loading and optional static linking
  - Full and high priority support of FOSS RDBMS: MySQL, PostgreSQL, Sqlite3
  - Support as many RDBMSs as possible via cppdb-odbc bridge
  - Simplicity in use
  - Locale safety
  - Support of both explicit verbose API and brief and nice syntactic sugar
 .
 This package contains the core library

Package: libcppdb-mysql0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: SQL Connectivity Library (MySQL backend)
 CppDB is an SQL connectivity library that is designed to provide platform and
 Database independent connectivity API similarly to what JDBC, ODBC and other
 connectivity libraries do.
 .
 This library is developed as part of CppCMS Project - the C++ Web Development
 Framework.
  .
 CppDB was designed with following goals in the mind:
  - Performance is the primary goal - make fastest possible SQL connectivity as
    possible
  - Transparent connection pooling support
  - Transparent prepared statements caching
  - Dynamic DB modules loading and optional static linking
  - Full and high priority support of FOSS RDBMS: MySQL, PostgreSQL, Sqlite3
  - Support as many RDBMSs as possible via cppdb-odbc bridge
  - Simplicity in use
  - Locale safety
  - Support of both explicit verbose API and brief and nice syntactic sugar
 .
 This package contains the MySQL backend

Package: libcppdb-sqlite3-0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: SQL Connectivity Library (sqlite3 backend)
 CppDB is an SQL connectivity library that is designed to provide platform and
 Database independent connectivity API similarly to what JDBC, ODBC and other
 connectivity libraries do.
 .
 This library is developed as part of CppCMS Project - the C++ Web Development
 Framework.
  .
 CppDB was designed with following goals in the mind:
  - Performance is the primary goal - make fastest possible SQL connectivity as
    possible
  - Transparent connection pooling support
  - Transparent prepared statements caching
  - Dynamic DB modules loading and optional static linking
  - Full and high priority support of FOSS RDBMS: MySQL, PostgreSQL, Sqlite3
  - Support as many RDBMSs as possible via cppdb-odbc bridge
  - Simplicity in use
  - Locale safety
  - Support of both explicit verbose API and brief and nice syntactic sugar
 .
 This package contains the sqlite3 backend

Package: libcppdb-postgresql0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: SQL Connectivity Library (PostgreSQL backend)
 CppDB is an SQL connectivity library that is designed to provide platform and
 Database independent connectivity API similarly to what JDBC, ODBC and other
 connectivity libraries do.
 .
 This library is developed as part of CppCMS Project - the C++ Web Development
 Framework.
  .
 CppDB was designed with following goals in the mind:
  - Performance is the primary goal - make fastest possible SQL connectivity as
    possible
  - Transparent connection pooling support
  - Transparent prepared statements caching
  - Dynamic DB modules loading and optional static linking
  - Full and high priority support of FOSS RDBMS: MySQL, PostgreSQL, Sqlite3
  - Support as many RDBMSs as possible via cppdb-odbc bridge
  - Simplicity in use
  - Locale safety
  - Support of both explicit verbose API and brief and nice syntactic sugar
 .
 This package contains the PostgreSQL backend

Package: libcppdb-odbc0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: SQL Connectivity Library (odbc backend)
 CppDB is an SQL connectivity library that is designed to provide platform and
 Database independent connectivity API similarly to what JDBC, ODBC and other
 connectivity libraries do.
 .
 This library is developed as part of CppCMS Project - the C++ Web Development
 Framework.
  .
 CppDB was designed with following goals in the mind:
  - Performance is the primary goal - make fastest possible SQL connectivity as
    possible
  - Transparent connection pooling support
  - Transparent prepared statements caching
  - Dynamic DB modules loading and optional static linking
  - Full and high priority support of FOSS RDBMS: MySQL, PostgreSQL, Sqlite3
  - Support as many RDBMSs as possible via cppdb-odbc bridge
  - Simplicity in use
  - Locale safety
  - Support of both explicit verbose API and brief and nice syntactic sugar
 .
 This package contains the odbc backend
